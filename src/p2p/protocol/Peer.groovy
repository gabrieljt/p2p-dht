package p2p.protocol

import p2p.app.UserInterface
import p2p.app.UserOptions
import p2p.dht.DHT
import p2p.local.FileManager
import p2p.network.Host
import p2p.network.transport.client.Client
import p2p.network.transport.multicast.Multicast
import p2p.network.transport.Message
import p2p.network.transport.MessageHeader
import p2p.network.transport.Marshaller
import p2p.network.transport.server.PeerServer
import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
class Peer extends UserInterface implements Runnable {
    static Host host
    Host superPeer
    Multicast multicast
    PeerType type
    final static FileManager sharedFiles = new FileManager("shared")
    final static FileManager downloadedFiles = new FileManager("downloads")
    final static DHT dhtFiles = new DHT()

    Peer(Host host, Multicast multicast, PeerType type) {
        this.host = host
        this.multicast = multicast
        this.type = type
    }

    @Override
    void run() {
        while(!superPeer) {
            if (discoverOrConnect())
                discoverNetwork()
            else {
                log.info("Digite o endereço IP do Super-Peer:")
                scan()
                superPeer = new Host(userInfo, host.port, "")
            }
        }
        // Conectar ao Super Peer
        boolean connected = attemptConnect()
        if (connected) {
            log.info("Conectado ao Super-Peer $superPeer")
            // Configurar e Inicializar Servidor
            PeerServer server = new PeerServer(host.port)
            def serverThread = Thread.start { server.run() }

            while (superPeer) {
                //TODO: publicar arquivos, procurar arquivos && download
                peerActions()
                //TODO: superPeer, are you there? (ping)
            }

            // Desconectou do Super Peer
            serverThread.join(100)
            server.freeResources()
        } else
            log.warn("Não foi possível se conectar ao Super-Peer, reinicializando descoberta da Rede...")
    }

    void discoverNetwork() {
        if (multicast) {
            log.info("Descobrindo Super-Peer...")
            sleep(multicast.heartbeat*1000)
            multicast.sendMessage = Marshaller.marshall(MessageHeader.PEER, host)
            multicast.run()
            multicast.receiveMessage = Marshaller.unmarshall(new String(multicast.buffer).trim())
            if (multicast.receiveMessage) {
                Message receivedMessage = (Message) multicast.receiveMessage
                if (receivedMessage.header.equals(MessageHeader.SUPER_PEER)) {
                    Host superPeer = (Host)(receivedMessage.payload)
                    if (wishToConnect(superPeer)) {
                        this.superPeer = superPeer
                    }
                }
            }
        } else {
            log.warn("Multicast desabilitado")
            System.exit(0)
        }
    }

    static boolean sendMessageTo(Host host, MessageHeader messageHeader, def payload, MessageHeader expectedResponse) {
        Client client = new Client(host)
        if (client.selectHostAndConnect(messageHeader, payload)) {
            Message receivedMessage = (Message) client.receiveMessage
            return receivedMessage.header.equals(expectedResponse)
        }
        return false
    }

    boolean attemptConnect() {
        int attempts = 0
        boolean connected = false
        while (!connected && attempts < 3) {
            sleep(3000)
            ++attempts
            log.info("Tentativa $attempts: conectando ao Super-Peer $superPeer...")
            connected = connectToSuperPeer()
        }
        return connected
    }

    boolean connectToSuperPeer() {
        host.isOnline = true
        return sendMessageTo(superPeer, MessageHeader.CONNECT, host, MessageHeader.ACK)
    }

    void peerActions() {
        mainMenu()
        switch (selectedOption) {
            case UserOptions.LIST_FILES:
                listFiles()
                break
            case UserOptions.PUBLISH:
                publishFile()
                break
            case UserOptions.QUERY:
                queryFile()
                break
            case UserOptions.DISCONNECT:
                attemptDisconnect()
                break
            case UserOptions.EXIT:
                attemptDisconnect()
                log.info("Finalizando P2P-DHT")
                System.exit(0)
        }
    }

    boolean attemptDisconnect() {
        int attempts = 0
        boolean disconnected = false
        while (!disconnected && attempts < 3) {
            sleep(3000)
            ++attempts
            log.info("Tentativa $attempts: desconectando do Super-Peer $superPeer...")
            disconnected = disconnectFromSuperPeer()
        }
        superPeer = null
        return disconnected
    }

    boolean disconnectFromSuperPeer() {
        host.isOnline = false
        return sendMessageTo(superPeer, MessageHeader.DISCONNECT, host, MessageHeader.ACK)
    }

    static boolean listFiles() {
        if (sharedFiles.isEmpty()) {
            log.info("Nenhum arquivo encontrado. Copie ou mova seus arquivos para o diretório: ${sharedFiles.dir.absolutePath}")
            dhtFiles.clear()
            return false
        } else {
            log.info("Arquivos Compartilhados")
            sharedFiles.listFiles().each {
                println(">> ${it.name}")
                if (!dhtFiles.get(it.name))
                    dhtFiles.put(it.name, host.address)
            }
            return true
        }
    }

    boolean publishFile() {
        if (listFiles()) {
            log.info("Digite o nome do arquivo que deseja compartilhar:")
            scan()
            File file = sharedFiles.findFile(userInfo)
            if (!file.exists()) {
                log.warn("Arquivo $userInfo não encontrado.")
                return false
            }
            return sendMessageTo(superPeer, MessageHeader.PUBLISH, file.name, MessageHeader.ACK)
        }
        return false
    }

    boolean queryFile() {
        log.info("Digite o nome do arquivo que deseja procurar:")
        scan()
        return sendMessageTo(superPeer, MessageHeader.QUERY, userInfo, MessageHeader.ACK)
    }

    void downloadFile() {

    }

    void uploadFile() {

    }
}
