package p2p.protocol

import p2p.dht.DHT
import p2p.network.Host
import p2p.network.HostManager
import p2p.network.transport.server.SuperPeerServer
import p2p.network.transport.Marshaller
import p2p.network.transport.MessageHeader
import p2p.network.transport.Message
import groovy.transform.InheritConstructors
import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@InheritConstructors
class SuperPeer extends Peer implements Runnable {
    final static HostManager peers = new HostManager()
    final static HostManager superPeers = new HostManager()
    final static DHT dhtRoute = new DHT()

    @Override
    void run() {
        addShutdownHook { sendGoodbye() }

        // Inicializar Servidor
        SuperPeerServer server = new SuperPeerServer(host.port)
        def serverThread = Thread.start { server.run() }

        // Inializar descoberta de Super Peers
        while (serverThread.alive) {
            discoverNetwork()
            sleep(multicast.heartbeat*1000)
        }
        serverThread.join(100)
    }

    @Override
    void discoverNetwork() {
        multicast.sendMessage = Marshaller.marshall(MessageHeader.SUPER_PEER, host)
        multicast.run()
        multicast.receiveMessage = Marshaller.unmarshall(new String(multicast.buffer).trim())
        if (multicast.receiveMessage) {
            Message receivedMessage = (Message) multicast.receiveMessage
            if (receivedMessage.header.equals(MessageHeader.SUPER_PEER)) {
                Host superPeer = (Host)(receivedMessage.payload)
                superPeers.addOrUpdateHost(superPeer)
                if (!dhtRoute.get(superPeer.address)) {
                    dhtRoute.put(superPeer.address, superPeer.address)
                }
            }
        }
    }

    void sendGoodbye() {
        if (multicast) {
            log.info("Enviando mensagem de despedida...")
            host.timestamp = new Date().toTimestamp().toString()
            host.isOnline = false
            multicast.sendMessage = Marshaller.marshall(MessageHeader.SUPER_PEER, host)
            multicast.run()
            multicast.freeResources()
        }
    }
}
