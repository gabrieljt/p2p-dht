package p2p.protocol

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/10/13
 * Time: 7:57 PM
 * To change this template use File | Settings | File Templates.
 */
public enum PeerType {
    PEER("PEER"),
    SUPER_PEER("SUPER_PEER"),

    final String value

    PeerType(String value) { this.value = value }

    public String toString() { value }
}