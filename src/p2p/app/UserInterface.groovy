package p2p.app

import groovy.util.logging.Slf4j
import p2p.network.Host

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/12/13
 * Time: 2:42 AM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
abstract class UserInterface {

    String userInfo = ""
    Scanner scanner = new Scanner(System.in)
    UserOptions selectedOption

    void mainMenu() {
        log.info("Digite uma opção:")
        println("[l]ist files : Listar arquivos compartilhados.")
        println("[p]ublish files : Publicar arquivo(s) compartilhado(s) para o Super-Peer.")
        println("[q]uery <file-name> : Procurar arquivo.")
        println("[d]isconnect : Desconectar deste Super-Peer.")
        println("[e]xit : Sair.")
        scan()
        switch (userInfo) {
            case "l":
                selectedOption = UserOptions.LIST_FILES
                break
            case "p":
                selectedOption = UserOptions.PUBLISH
                break
            case "q":
                selectedOption = UserOptions.QUERY
                break
            case "d":
                selectedOption = UserOptions.DISCONNECT
                break
            case "e":
                selectedOption = UserOptions.EXIT
                break
            default:
                log.warn("Nenhuma opção escolhida.")
                selectedOption = null
        }
    }

    boolean wishToConnect(Host host) {
        while (!(userInfo.equals("s") || userInfo.equals("n"))) {
            log.info("Deseja se conectar ao host: ${host} ? [s/n]")
            scan()
        }
        return userInfo.equals("s")
    }

    boolean discoverOrConnect() {
        while (!(userInfo.equals("d") || userInfo.equals("c"))) {
            log.info("Deseja [d]escobrir Super-Peer ou [c]onectar-se diretamente? [d/c]")
            scan()
        }
        return userInfo.equals("d")
    }

    void scan() {
        try {
            if (scanner.hasNextLine())
                userInfo = scanner.nextLine()
        } catch (NoSuchElementException e) {
            log.warn(e.message)
        }
    }

    void idle() {
        while (true) {
            print('.')
            sleep(1000)
        }
    }
}

public enum UserOptions {
    LIST_FILES("LIST_FILES"),
    PUBLISH("PUBLISH"),
    QUERY("QUERY"),
    DISCONNECT("DISCONNECT"),
    EXIT("EXIT")

    final String value

    UserOptions(String value) { this.value = value }

    public String toString() { value }
}

