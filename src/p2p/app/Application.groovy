package p2p.app

import p2p.network.transport.multicast.Multicast
import p2p.network.Host
import p2p.protocol.Peer
import p2p.protocol.SuperPeer
import p2p.protocol.PeerType
import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:36 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
class Application {

    static boolean superPeer = false

    static int port = 22222
    static String multicastGroup = "224.0.0.1" //all hosts
    static int multicastPort = 33333
    static int multicastHeartbeat = 10
    static int multicastBufferSize = 1024
    static int multicastTimeToLive = 3
    static boolean peerMulticast = true


    static def userInterface

    static void main(args) {

        userInterface = new CliBuilder(usage: 'java -jar p2p-dht-<version>.jar [args]' )
        userInterface.h(longOpt: 'help', 'informações de uso', required: false)
        userInterface.sp(longOpt: 'super-peer', 'modo super-peer', required: false)
        userInterface.npm(longOpt: 'no-peer-multicast', 'sem multicast, apenas no modo peer', required: false)

        OptionAccessor opt = userInterface.parse(args)
        if (!opt) {
            System.exit(1)
        }
        // print usage if -h, --help
        if (opt.h) {
            userInterface.usage()
            System.exit(0)
        }

        if (opt.sp) superPeer = true
        if (opt.npm) peerMulticast = false

        def initThread = Thread.start { init() }
        initThread.join()
        System.exit(0)
    }

    static void init() {

        log.info("Inicializando P2P-DHT")
        //Configurando Host e Multicast
        String hostAddress = ""
        String hostName = InetAddress.localHost.hostName
        Host host
        Multicast multicast = null

        for (NetworkInterface networkInterface in NetworkInterface.networkInterfaces)
            for (InetAddress inetAddress in networkInterface.inetAddresses)
                if (inetAddress.hostAddress.startsWith("192.") || inetAddress.hostAddress.startsWith("10."))
                    hostAddress = inetAddress.hostAddress

        if (!(hostAddress && hostName))
            System.exit(1)

        host = new Host(hostAddress, port, hostName)
        log.info("Informações do Host: $host")

        while (true) {
            if (superPeer) {
                multicast = new Multicast(multicastGroup, multicastPort, multicastHeartbeat, multicastBufferSize, multicastTimeToLive)
                log.info("Inicializando modo Super-Peer")
                SuperPeer superPeer = new SuperPeer(host, multicast, PeerType.SUPER_PEER)
                def superPeerThread = Thread.start{ superPeer.run() }
                superPeerThread.join()
                System.exit(0)
            } else {
                if (peerMulticast)
                    multicast = new Multicast(multicastGroup, multicastPort, multicastHeartbeat, multicastBufferSize, multicastTimeToLive)
                log.info("Inicializando modo Peer")
                Peer peer = new Peer(host, multicast, PeerType.PEER)
                def peerThread = Thread.start{ peer.run() }
                peerThread.join()
                sleep(1000)
            }
        }
    }
}
