package p2p.local

import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/12/13
 * Time: 1:36 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
class FileManager {
    final File dir
    final String FILES_PATH

    FileManager(String dirName) {
        dir = new File(dirName)
        if (dir.exists() && !dir.isDirectory())
            dir.delete()
        if (!dir.exists())
            dir.mkdir()
        FILES_PATH = dir.path + File.separator
    }

    boolean isEmpty() {
        return dir.list().length == 0
    }

    List<File> listFiles() {
        return dir.listFiles()
    }

    File findFile(String fileName) {
        String filePath = FILES_PATH + fileName
        return new File(filePath)
    }

    boolean createFile(String fileName, ArrayList fileBytes) {
        FileInputStream fileInputStream
        File file = new File(FILES_PATH + fileName)
        file.exists() ? file.delete() && file.createNewFile() : file.createNewFile()
        byte[] bytes = new byte[fileBytes.size()]
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) fileBytes.get(i)
        }
        try {
            fileInputStream = new FileInputStream(file)
            fileInputStream.read(bytes)
            fileInputStream.close()

            FileOutputStream fileOuputStream = new FileOutputStream(FILES_PATH + fileName)
            fileOuputStream.write(bytes)
            fileOuputStream.close()

            log.info("Download do arquivo $fileName sucedido.")
            true
        }catch(Exception e){
            log.warn("Erro em download do arquivo $fileName.")
            e.printStackTrace();
            false
        }
    }
}
