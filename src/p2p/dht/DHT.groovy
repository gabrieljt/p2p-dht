package p2p.dht

import java.security.MessageDigest

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/12/13
 * Time: 7:59 PM
 * To change this template use File | Settings | File Templates.
 */
class DHT {

    def hashTable = [:]

    static String generateMD5(String s) {
        MessageDigest digest = MessageDigest.getInstance("MD5")
        digest.update(s.bytes);
        new BigInteger(1, digest.digest()).toString(10).padLeft(32, '0').substring(0, 9)
    }

    def put(String key, String value) {
        key = generateMD5(key)
        hashTable.put(key, value)
    }

    def get(String key) {
        key = generateMD5(key)
        hashTable.get(key)
    }

    def lookup(String key) {
        key = generateMD5(key)
        def sortedTable = hashTable.sort { it.key }
        def successor = sortedTable.find { it.key > key }
        successor ? successor.value : sortedTable.min { it.key }.value
    }

    def clear() {
        hashTable.clear()
    }
}
