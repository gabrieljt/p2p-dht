package p2p.network


/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:46 PM
 * To change this template use File | Settings | File Templates.
 */
class Host {
    String address
    int port
    String name
    String timestamp
    boolean isOnline = true

    Host() {}

    Host(String address, int port, String name) {
        this.address = address
        this.port = port
        this.name = name
        this.timestamp = new Date().toTimestamp().toString()
    }

    String toString() {
        return "[Nome: $name, Endereço IP: $address, Timestamp: $timestamp]"
    }
}
