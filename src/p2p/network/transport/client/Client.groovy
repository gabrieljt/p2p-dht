package p2p.network.transport.client

import p2p.network.Host
import p2p.network.transport.Marshaller
import p2p.network.transport.Message
import p2p.network.transport.MessageHeader
import p2p.network.transport.TCPConnection
import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:51 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
class Client extends TCPConnection {
    Host server

    Client(Host server) {
        this.server = server
    }

    boolean selectHost() {
        try {
            socket = new Socket(server.address, server.port)
            output = new PrintWriter(socket.getOutputStream(), true)
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        } catch (UnknownHostException e) {
            log.warn("Erro conectar ao host: $e.message")
            return false
        } catch (IOException e) {
            log.warn("Erro conectar ao host: $e.message")
            return false
        }
        return true
    }

    @Override
    void connect() {
        String receivedMessageString
        try {
            //Enviando mensagem
            output.println(sendMessage)
            //Recebendo mensagem
            receivedMessageString = input.readLine()
            //Verificando mensagem
            if (receivedMessageString) {
                receiveMessage = Marshaller.unmarshall(receivedMessageString)
            }
            //OK, liberando recursos
            freeResources()
            if (receiveMessage) {
                Message sentMessage = (Message) Marshaller.unmarshall(sendMessage.toString())
                Message receivedMessage = (Message) receiveMessage
                log.info ("Mensagem enviada: $sentMessage")
                log.info("Mensagem recebida: $receivedMessage")
            }
        } catch (Exception e) {
            def sentMessage = sendMessage
            def receivedMessage = receiveMessage
            if (!(sentMessage || receivedMessage))
                log.warn("Erro Transmissão: $e.message")
            else {
                log.info ("Mensagem enviada: $sentMessage")
                log.info("Mensagem recebida: $receivedMessage")
            }
        }
    }

    boolean selectHostAndConnect(MessageHeader messageHeader, def payload) {
        if (selectHost()) {
            sendMessage = Marshaller.marshall(messageHeader, payload)
            connect()
            if (receiveMessage) {
                return true
            }
        }
        return false
    }
}
