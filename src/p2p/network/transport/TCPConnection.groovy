package p2p.network.transport

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/10/13
 * Time: 7:59 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class TCPConnection {
    Socket socket
    BufferedReader input
    PrintWriter output
    def sendMessage
    def receiveMessage

    TCPConnection() {}

    TCPConnection(Socket clientSocket) {
        this.socket = clientSocket
        this.output = new PrintWriter(socket.getOutputStream(), true)
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()))
    }

    abstract void connect()

    void freeResources() {
        if (socket && !socket.isClosed())
            socket.close()

        if (input)
            input.close()

        if (output)
            output.close()
    }
}