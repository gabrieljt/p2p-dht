package p2p.network.transport

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.json.JsonException
import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
class Marshaller {

    static def marshall(MessageHeader header, def payload) {
        return new JsonBuilder(new Message(header, payload))
    }

    static def unmarshall(String payload) {
        try {
            return new JsonSlurper().parseText(payload)
        }catch (JsonException e) {
            log.warn("Não foi possível interpretar mensagem recebida: $e.message")
            return null
        }
    }
}
