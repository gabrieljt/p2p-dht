package p2p.network.transport

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */

public class Message {
    MessageHeader header
    def payload
    String timestamp

    Message() {}

    Message(MessageHeader header, def payload) {
        this.header = header
        this.payload = payload
        this.timestamp = new Date().toTimestamp().toString()
    }

    String toString() {
        return "[Header: $header, Payload: $payload, Timestamp: $timestamp]"
    }
}

public enum MessageHeader {
    PEER("PEER"),
    SUPER_PEER("SUPER_PEER"),
    ACK("ACK"),
    CONNECT("CONNECT"),
    DISCONNECT("DISCONNECT"),
    PUBLISH("PUBLISH"),
    QUERY("QUERY"),
    PUT("PUT"),
    GET("GET"),
    DOWNLOAD("DOWNLOAD"),
    UPLOAD("UPLOAD"),
    ERROR("ERROR")

    final String value

    MessageHeader(String value) { this.value = value }

    public String toString() { value }
}

