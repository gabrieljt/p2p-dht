package p2p.network.transport.server

import groovy.transform.InheritConstructors
import groovy.util.logging.Slf4j
import p2p.network.Host
import p2p.network.transport.Marshaller
import p2p.network.transport.Message
import p2p.network.transport.MessageHeader
import p2p.network.transport.TCPConnection
import p2p.protocol.SuperPeer

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/10/13
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
@InheritConstructors
class SuperPeerServer extends Server implements Runnable {

    @Override
    void run() {
        addShutdownHook { freeResources() }

        def serverThread
        while (true) {
            if (accept()) {
                serverThread = new Thread(clientSocket.inetAddress.hostAddress)
                serverThread.start { new SuperPeerHandler(clientSocket).run() }
                serverThreads.add(serverThread)
            }
        }
    }
}

@Slf4j
@InheritConstructors
class SuperPeerHandler extends TCPConnection implements Runnable {
    Host connectedPeer
    Thread identifyPeerThread

    @Override
    void run() {
        addShutdownHook { freeResources() }

        log.info("Peer conectado: [${socket.inetAddress.hostAddress}]")
         identifyPeerThread = Thread.start {
            log.info("Identificando Peer...")
            connectedPeer = findPeerOrSuperPeer()
            connectedPeer ? log.info("Peer identificado: $connectedPeer") : log.warn("Peer não identificado.")
        }
        connect()
        identifyPeerThread.join()
    }

    Host findPeerOrSuperPeer() {
        Host peer = SuperPeer.peers.findHost(socket.inetAddress.hostAddress)
        if (!peer)
            peer = SuperPeer.superPeers.findHost(socket.inetAddress.hostAddress)
        return peer
    }

    void waitIdentifyPeerThread() {
        while (identifyPeerThread.alive)
            sleep(100)
    }

    @Override
    void connect() {
        String receiveMessageString
        try {
            //Recebendo mensagem
            receiveMessageString = input.readLine()

            if (receiveMessageString) {
                receiveMessage = Marshaller.unmarshall(receiveMessageString)
            }

            if (receiveMessage) {
                //Processando mensagem
                receive()
                //Enviando resposta
                output.println(sendMessage)
                freeResources()
                Message sentMessage = (Message) Marshaller.unmarshall(sendMessage.toString())
                Message receivedMessage = (Message) receiveMessage
                log.info("Mensagem recebida: $receivedMessage")
                log.info ("Mensagem enviada: $sentMessage")
            } else
                receiveMessage = null
        } catch (Exception e) {
            receiveMessage = null
            log.warn("Erro Transmissão: $e.message")
        }
    }

    void receive() {
        Message message = (Message) receiveMessage
        switch (message.header) {
            case MessageHeader.CONNECT:
            case MessageHeader.DISCONNECT:
                Host peer = (Host) message.payload
                SuperPeer.peers.addOrUpdateHost(peer)
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Operação ${message.header} efetivada")
                break
            case MessageHeader.PUBLISH:
                waitIdentifyPeerThread()
                String fileName = (String)message.payload
                String destination = SuperPeer.dhtRoute.lookup(fileName)
                SuperPeer.sendMessageTo(SuperPeer.superPeers.findHost(destination), MessageHeader.PUT, [fileName: fileName, peerAddress: connectedPeer.address], MessageHeader.ACK)
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Operação ${message.header} efetivada")
                break
            case MessageHeader.PUT:
                waitIdentifyPeerThread()
                def data = message.payload
                SuperPeer.dhtFiles.put(data.fileName, data.peerAddress)
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Operação ${message.header} efetivada")
                break
            case MessageHeader.QUERY:
                waitIdentifyPeerThread()
                String fileName = (String)message.payload
                String destination = SuperPeer.dhtRoute.lookup(fileName)
                SuperPeer.sendMessageTo(SuperPeer.superPeers.findHost(destination), MessageHeader.GET, [fileName: fileName, peerAddress: connectedPeer.address], MessageHeader.ACK)
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Operação ${message.header} efetivada")
                break
            case MessageHeader.GET:
                waitIdentifyPeerThread()
                def data = message.payload
                String fileLocation = SuperPeer.dhtFiles.get(data.fileName)
                if (fileLocation)
                    SuperPeer.sendMessageTo(new Host(address: data.peerAddress, port: SuperPeer.host.port), MessageHeader.QUERY, [fileName: data.fileName, peerAddress: fileLocation], MessageHeader.ACK)
                else
                    SuperPeer.sendMessageTo(new Host(address: data.peerAddress, port: SuperPeer.host.port), MessageHeader.ERROR, "Arquivo ${data.fileName} não encontrado", MessageHeader.ACK)
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Operação ${message.header} efetivada")
                break
            default:
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Silence is gold...")
        }
    }

    void connectOrDisconnectPeer() {

    }

    void disconnectPeer() {

    }

    void registerFiles() {

    }

    void findFile() {

    }
}
