package p2p.network.transport.server

import groovy.util.logging.Slf4j

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/7/13
 * Time: 10:51 PM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
abstract class Server {
    int port
    ServerSocket serverSocket
    Socket clientSocket
    ArrayList<Thread> serverThreads = []
    int serverThreadsLimit = 30

    Server() {}

    Server(int port) {
        this.port = port
        listen()
    }

    void listen() {
        try {
            serverSocket = new ServerSocket(port)
            log.info("Escutando na porta $port")
        } catch (IOException e) {
            log.error("Erro ao escutar na porta $port")
            System.exit(1)
        }
    }

    boolean accept() {
        try {
            clientSocket = serverSocket.accept()
        } catch (IOException e) {
            return false
        }
        def serverThread = serverThreads.find { it.name.equals(clientSocket.inetAddress.hostAddress) }
        if (serverThread) {
            if (serverThread.alive) {
                log.warn("Host $clientSocket.inetAddress.hostAddress enviando múltiplas requisições.")
                return false
            } else {
                serverThreads.remove(serverThread)
            }
        }
        if (serverThreads.size() >= serverThreadsLimit) {
            for (thread in serverThreads) {
                thread.join()
                serverThreads.remove(thread)
            }
        }
        return true
    }

    void freeResources() {
        if (serverSocket && !serverSocket.isClosed())
            serverSocket.close()

        if (clientSocket && !clientSocket.isClosed())
            clientSocket.close()
    }
}
