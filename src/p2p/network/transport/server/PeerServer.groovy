package p2p.network.transport.server

import groovy.transform.InheritConstructors
import groovy.util.logging.Slf4j
import p2p.network.Host
import p2p.network.transport.Marshaller
import p2p.network.transport.Message
import p2p.network.transport.MessageHeader
import p2p.network.transport.TCPConnection
import p2p.protocol.Peer

/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/10/13
 * Time: 12:27 PM
 * To change this template use File | Settings | File Templates.
 */
@InheritConstructors
class PeerServer extends Server implements Runnable {

    @Override
    void run() {
        addShutdownHook { freeResources() }

        def serverThread
        while (true) {
            if (accept()) {
                serverThread = new Thread(clientSocket.inetAddress.hostAddress)
                serverThread.start { new PeerHandler(clientSocket).run() }
                serverThreads.add(serverThread)
            }
        }
    }
}

@Slf4j
@InheritConstructors
class PeerHandler extends TCPConnection implements Runnable {

    @Override
    void run() {
        addShutdownHook { freeResources() }

        log.info("Peer conectado: [${socket.inetAddress.hostAddress}]")
        connect()
    }

    @Override
    void connect() {
        String receiveMessageString
        try {
            //Recebendo mensagem
            receiveMessageString = input.readLine()

            if (receiveMessageString) {
                receiveMessage = Marshaller.unmarshall(receiveMessageString)
            }

            if (receiveMessage) {
                //Processando mensagem
                receive()
                //Enviando resposta
                output.println(sendMessage)
                freeResources()
                Message receivedMessage = (Message) receiveMessage
                Message sentMessage = (Message) Marshaller.unmarshall(sendMessage.toString())
                log.info("Mensagem recebida: $receivedMessage")
                log.info ("Mensagem enviada: $sentMessage")
            }
        } catch (Exception e) {
            def sentMessage = sendMessage
            def receivedMessage = receiveMessage
            if (!(sentMessage || receivedMessage))
                log.warn("Erro Transmissão: $e.message")
            else {
                log.info ("Mensagem enviada: $sentMessage")
                log.info("Mensagem recebida: $receivedMessage")
            }
        }
    }

    void receive() {
        Message message = (Message) receiveMessage
        switch (message.header) {
            case MessageHeader.ERROR:
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Mensagem ${message.header} recebida")
                break
            case MessageHeader.QUERY:
                def data = message.payload
                log.info("Arquivo ${data.fileName} encontrado. Peer: $data.peerAddress")
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Mensagem ${message.header} recebida")
                Thread.start {
                    Peer.sendMessageTo(new Host(address: data.peerAddress, port: Peer.host.port), MessageHeader.DOWNLOAD, [fileName: data.fileName, peerAddress: Peer.host.address], MessageHeader.ACK)
                }
                break
            case MessageHeader.DOWNLOAD:
                def data = message.payload
                if (Peer.dhtFiles.get(data.fileName)) {
                    sendMessage = Marshaller.marshall(MessageHeader.ACK, "Arquivo ${data.fileName} encontrado; inicializando Upload")
                    File uploadFile = Peer.sharedFiles.findFile(data.fileName)
                    Thread.start {
                        Peer.sendMessageTo(new Host(address: data.peerAddress, port: Peer.host.port), MessageHeader.UPLOAD, [fileName: data.fileName, fileBytes: uploadFile.bytes], MessageHeader.ACK)
                    }
                } else
                    sendMessage = Marshaller.marshall(MessageHeader.ERROR, "Arquivo ${data.fileName} não encontrado")
                break
            case MessageHeader.UPLOAD:
                def data = message.payload
                Peer.downloadedFiles.createFile(data.fileName, data.fileBytes)
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Download do arquivo ${data.fileName} realizado com sucesso")
                break
            default:
                sendMessage = Marshaller.marshall(MessageHeader.ACK, "Silence is gold...")
        }
    }
}
