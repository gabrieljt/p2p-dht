package p2p.network

import java.sql.Timestamp
import groovy.util.logging.Slf4j
/**
 * Created with IntelliJ IDEA.
 * User: gabriel
 * Date: 10/12/13
 * Time: 1:45 AM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
class HostManager {
    List<Host> hosts = []

    synchronized Host addHost(Host host) {
        if (hosts.add(host))
            log.info("Host adicionado: $host")
        else {
            log.warn("Host não adicionado: $host")
            host = null
        }
        return host
    }

    synchronized Host removeHost(Host host) {
        if (hosts.remove(host))
            log.info("Host removido: $host")
        else {
            log.warn("Host não removido: $host")
            host = null
        }
        return host
    }

    synchronized Host updateHost(Host newHost) {
        Host host = findHost(newHost)
        if (host) {
            host.timestamp = newHost.timestamp
            host.name = newHost.name
            log.info("Host atualizado: $host")
            newHost.isOnline ? activateHost(host) : inactivateHost(host)
        }
    }

    synchronized Host activateHost(Host host) {
        host.isOnline = true
        log.info("Host Online: $host")
        return host
    }

    synchronized Host inactivateHost(Host host) {
        host.isOnline = false
        log.info("Host Offline: $host")
        return host
    }

    Host findHost(Host host) {
        return hosts.find { it.address.equals(host.address) }
    }

    Host findHost(String address) {
        return hosts.find { it.address.equals(address) }
    }

    List<Host> getOnlineHosts() {
        return hosts.findAll { it.isOnline }
    }

    List<Host> getOfflineHosts() {
        return hosts.findAll { !it.isOnline }
    }

    int getOnlineHostsCount() {
        return getOnlineHosts().size()
    }

    int getOfflineHostsCount() {
        return getOfflineHosts().size()
    }

    Host addOrUpdateHost(Host host) {
        Host activeHost
        activeHost = findHost(host)
        if (!activeHost && host.isOnline) {
            return addHost(host)
        } else if (Timestamp.valueOf(host?.timestamp) > Timestamp.valueOf(activeHost?.timestamp) || !host.isOnline.equals(activeHost.isOnline)) {
            return updateHost(host)
        }
        return activeHost
    }
}
