P2P-DHT
-----

- Configurando o projeto:

	Siga as instruções de acordo com o seu Sistema Operacional.

	- (Apenas para Execução) Instale o JRE 1.7:
	- (Para Execução e os demais) Instale o JDK 1.7: http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
	- (Para Compilação) Instale o Gradle: www.gradle.org
	- (Para Desenvolvimento) Instale o Git: http://git-scm.com/
	- (Para Debug) Instale o Intellij IDEA Community Edition (IDE): http://www.jetbrains.com/idea/download/
	- Faça o download do projeto e descompacte, ou clone o repositório git no diretório de sua preferência.
		- Caso tenha o Git instalado, clique no botão 'clone' desta página para obter o comando necessário (terminal no Linux, Git Bash no windows).
		- Para download, clique em 'Downloads' nesta página e depois na aba 'Branches', selecione o master.zip por exemplo.
	- Inicialize o IDEA.				
	- Importe o projeto utilizando o Gradle como opção. Talvez seja necessário fornecer o diretório onde o Gradle foi instalado (Gradle Home) e o arquivo 'build.gradle' (Gradle Project) localizado na raíz do projeto.

- Compilando e executando o programa:
	
	Esta compilação gera apenas um JAR executável e não é necessária para executar a aplicação no IDEA.
	
	- Por linha de comando, navegue até a raíz do projeto.
	- Execute o comando 'gradle makejar'.
	- O JAR poderá ser encontrado no diretório 'build/libs/' e executado com o comando 'java -jar p2p-dht.jar'.

- Executando o programa no IDEA:

	- Após configurar o projeto, clique em 'Run >> Edit Configurations'.
	- Clique no botão '+' e selecione 'Groovy'.
	- Forneca o arquivo 'Application.groovy' como 'Script Path', localizado em 'src/p2p/app/'. É possível utilizar o botão '...' para encontrar o arquivo.
	- Forneça o módulo 'p2p-dht' da lista 'Module'.
	- Forneça a raíz do projeto como 'Working directory', se necessário.
	- Forneça um nome para o seu 'Run Configuration'.
	- Clique em 'Apply' e 'OK'.
	- Clique em 'Run' e escolha a a configuração criada.
	- Talvez seja necessário fornecer o diretório onde está instalado o JDK.

- Quaisquer dúvidas, entrar em contato.	

-----

- Abstrato:

	"Os clientes se associam a um SuperNodo e para gravarem ou recuperarem um arquivo, se comunicam com seu SuperNodo que busca, via DHT, onde está ou para onde vai o arquivo." -Trevelin, L. C.

-----

- O que já tem pronto?

	- Inicialização
		- Modo Peer: java -jar p2p-dht.jar
		- Modo SuperPeer: java -jar p2p-dht.jar -sp
		- IDEA: criar duas 'Run Configurations'. Fornecer parâmetro '-sp' para uma delas.

	- Descoberta da Rede		
		- Modo Peer:
		    - Envia multicast de um Message com o seu objeto Host como payload e com o MessageHeader.PEER como header.
		    - O Peer apenas processa multicasts com o cabeçalho da mensagem igual a SUPER_PEER, e ao receber tal mensagem o usuário pode escolher se conectar ou não ao SuperPeer.
		    - O procedimento continua até que o usuário decida se conectar ao SuperPeer. Caso não se conecte, retorna ao passo 1.
		    - Desta forma, associação à um SuperPeer torna-se descentralizada.

        - Modo SuperPeer:
            - Envia multicast de mensagem com cabeçalho SUPER_PEER e suas informações de Host como payload, enquanto estiver ativo.
            - O SuperPeer apenas processa multicasts com o cabeçalho da mensagem igual a SUPER_PEER, e ao receber tal mensagem atualiza a sua lista de SuperPeers conhecidos, se necessário.
            - Desta forma, os SuperPeers mantém um overlay da rede, pois eles também possuem uma lista de Peers conectados além de conhecerem os seus vizinhos SuperPeers.

    - Associação com SuperPeer:
        No modo Peer, após o usuário solicitar conectar-se ao SuperPeer, o Peer envia uma mensagem com o cabeçalho CONNECT e suas informações de Host como payload.
        O SuperPeer ao receber a mensagem atualiza sua lista de Peers e responde com um ACK.

    - Interface Usuário:
        No modo Peer, usuário pode escolher publicar seus arquivos, pesquisar um arquivo, desconectar-se do SuperPeer ou sair do programa.

    - Desassociação com SuperPeer:
        No  modo Peer, ao escolher esta opção, o Peer envia uma mensagem com o cabeçalho DISCONNECT e suas informações de Host como payload.
        O SuperPeer ao receber a mensagem atualiza sua lista de Peers e responde com um ACK.

-----

- Próximos Releases:

    - Mapear FileDescriptors compartilhados.

    - Publicar FileDescriptors ao SuperPeer.

    - Armazenar FileDescriptor no SuperPeer resultante da função hash (DHT).

-----

v0.2 master branch
:w